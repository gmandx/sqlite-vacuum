use std::{iter::Iterator, path::PathBuf};

use clap::Parser;

#[derive(Parser, Debug)]
#[clap(name = "sqlite-vacuum")]
pub struct Arguments {
    pub directories: Vec<PathBuf>,

    #[clap(long, action)]
    pub aggresive: bool,
}
